package com.kavworks.plugin;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;
import android.app.ActionBar;
import android.widget.Toast;
import android.view.View;

public class Actionbarhide extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {

        if (action.equals("hide")) {

            //String name = data.getString(0);
            //String message = "Hello, " + name;
            
            
            
            
            cordova.getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try{
							
							cordova.getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
								| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
								| View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
								// remove the following flag for version < API 19
								| View.SYSTEM_UI_FLAG_IMMERSIVE);
						
								//Toast toast = Toast.makeText(cordova.getActivity().getApplicationContext(), "hello", Toast.LENGTH_SHORT);
								//toast.show();
												
							ActionBar actionBar = cordova.getActivity().getActionBar();
							if(actionBar!=null && actionBar.isShowing()){
								actionBar.hide();
							}
							
					}catch(Exception e){
					}
					
					//callbackContext.success(); // Thread-safe.
				}
			});
            
            callbackContext.success("succeeded");

            

            return true;

        } else {
            
            return false;

        }
    }
}
