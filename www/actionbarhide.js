/*global cordova, module*/

module.exports = {
    hide: function (successCallback, errorCallback) {
		
		if(!successCallback){successCallback=function(){}}
		if(!errorCallback){errorCallback=function(){}}
		
        cordova.exec(successCallback, errorCallback, "Actionbarhide", "hide", [""]);
    }
};
